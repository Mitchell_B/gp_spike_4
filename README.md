# Spike Report

## GRAPHICS PROGRAMMING - Outdoor Lighting

### Introduction

In this spike we will explore 3 different types of outdoor lighting: static, stationary, and moveable.

### Goals

- A large landscape scene a large amount (100+) of low-poly foliage (use online assets) and an Atmospheric Fog Actor. Use the Third Person Character blueprint. We will create several copies so that we can compare and contrast the different lighting mobilities. The lighting setups for each copy is slightly different:
- Static level:
    - Static Sky Light, Static Directional Light, and a Sky Sphere
    - Create several sub-levels which will be Lighting Scenarios for Morning, Midday, Afternoon/Evening, and Night, and a way to toggle the active one in the Level Blueprint
- Stationary level:
    - Stationary Sky Light, Stationary Directional Light, and a Sky Sphere
    - Create several sub-levels which will be Lighting Scenarios for Morning, Midday, Afternoon/Evening, and Night, and a way to toggle the active one in the Level Blueprint
- Moveable level:
    - Moveable Sky Light, Movable Directional Light, a Sky Sphere
    - Have the level blueprint rotate the directional light and update all relevant actors to have the time of day change (~1 day/night cycle per 2 minutes).
    - Consider moving the fog for different parts of the day (just using a spline or sine wave).
    - Use Distance Field Ambient Occlusion

### Personnel

* Primary - Mitchell Bailey
* Secondary - Bradley Eales

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

* [Low-Poly Model](https://www.cgtrader.com/free-3d-models/plant/leaf/tree2-low-poly)
* [Sub-Level Creation (Static & Stationary Levels)](https://answers.unrealengine.com/questions/512084/who-can-explain-of-lighting-scenario-feature.html)
* [Creating a Day/Night Cycle (Moveable Level)](https://www.unrealengine.com/en-US/blog/simulating-area-lights-in-ue4)

### Tasks Undertaken

#### Static Level

1. Created a new third person template with no starter content.
1. Created a new empty level, called Static_Level, and put in unlit mode.
1. Placed a TemplateFloor from third person content in the level, added grassy material and low-poly scenery.
1. Placed ThirdPersonCharacter in scene and allowed to be possessed by Player 0.
1. Followed the 'Sub-Level Creation' guide to create the necessary sub-levels (Morning, Midday, Afternoon, Night).
1. Opened each sub-level and placed a directional light, sky light, and BP_Sky_Sphere in the scene.
    - Set the directional light and sky light as static, rather than their default stationary.
    - In the BP_Sky_Sphere details panel, set the directional light actor to be the directional light in the current scene.
    - For each sub-level, adjusted angle of directional light to match the time of day the scene represents.
1. Opened the level blueprint of the main level (Persistent Level) and allowed it to load the different sub-levels when keys 1, 2, 3, and 4 are pressed.
    - Used Load Stream Level and Unload Stream Level in order to decide which levels to load and which levels to potentially unload if needed.
    - Used Load Stream Level from Event Begin Play so level does not start in darkness.

#### Stationary Level

1. Duplicated the main level, Static_Level, and renamed it to Stationary_Level.
1. Repeated steps for the static level, except kept all directional lights and sky lights as stationary instead of changing them to static.

#### Moveable Level

1. Duplicated the main level, Stationary_Level, and renamed it to Moveable_Level.
1. Opened Level panel, and removed all sub-levels from it.
1. Added directional light, sky light, and BP_Sky_Sphere directly in Moveable_Level, and set them to be moveable rather than static or stationary.
    - Once again, made sure the directional light was referenced in the BP_Sky_Sphere details panel.
1. Opened the level blueprint and made the directional light and BP_Sky_Sphere rotate on tick.
    - Got a reference to BP_Sky_Sphere and connected it to 'Update Sun Direction'.
    - Got a reference to the directional light and connected it to 'AddActorLocalRotation'.
    - Added a 'Make Rotator' and connected a 'float + float' to the pitch input, then connect the 'Make Rotator' output to the 'AddActorLocalRotation'.
    - Connected the Event Tick delta seconds output to the first input of the 'float + float'.
    - Created a new float variable called 'SunSpeed', value set to 1, and connected it to the second input of the 'float + float'.

### What we found out

- How sub-levels are used to create different lighting scenarios in one level without having to create 4 seperate persistent levels.
- The differences between static, stationary, and moveable lighting and the affect they can have on a level.
- How to create a day/night cycle using blueprints.

### Open Issues/Risks

- When using sub-levels as lighting scenarios for a level, ensure you are placing the light actors into the sub-level, not the persistent level.
- When building the lighting for each sub-level, ensure that only the sub-level you want to build is visible.
    - This can be checked in the levels panel.